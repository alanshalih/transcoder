const NodeMediaServer = require('./');
const axios = require('axios')
const fs = require('fs')
require('dotenv').config()
const config = {
  rtmp: {
    port: process.env.RTMP_PORT,
    chunk_size: 60000,
    gop_cache: true,
    ping: 30,
    ping_timeout: 60, 
  },
  http:{
    mediaroot:  process.env.MEDIAROOT,
    port : '2005'
  }, 
  fission: {
    
    ffmpeg: process.env.FFMPEG,
    tasks: [
      {
        destination_rtmp : process.env.DESTINATION_RTMP,
        rule: "show/*",
        model: [ 
          {
            ab: "128k",
            vb: "1500k",
            vs: "1280x720",
            vf: "30",
          },
          {
            ab: "96k",
            vb: "1000k",
            vs: "854x480",
            vf: "24",
          },
          {
            ab: "96k",
            vb: "600k",
            vs: "640x360",
            vf: "20",
          },
        ]
      }
    ]
  }
};


let nms = new NodeMediaServer(config)
nms.run();

nms.on('preConnect', (id, args) => {
  console.log('[NodeEvent on preConnect]', `id=${id} args=${JSON.stringify(args)}`);
  // let session = nms.getSession(id);
  // session.reject();
});

nms.on('postConnect', (id,StreamPath, args) => {
  
  console.log('[NodeEvent on postConnect]', `id=${id} args=${JSON.stringify(args)}`);
});

nms.on('doneConnect', (id, args) => {
  console.log('[NodeEvent on doneConnect]', `id=${id} args=${JSON.stringify(args)}`);
});

nms.on('prePublish', (id, StreamPath, args) => {
  console.log(StreamPath) 
  const name = StreamPath.split('/')[2]
  console.log(name)
let text = `
#EXTM3U
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=1000000
index_360.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=2500000
index_480.m3u8
#EXT-X-STREAM-INF:PROGRAM-ID=1, BANDWIDTH=4000000
index_720.m3u8
` 
const dir = process.env.MEDIAROOT+'/'+name;
if (!fs.existsSync(dir)){
  fs.mkdirSync(dir);
}
fs.writeFile(process.env.MEDIAROOT+'/'+name+'/index.m3u8', text.replace('\n',''), function (err) {
  if (err) throw err;
  console.log('Saved!');
});
  // console.log('[NodeEvent on prePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
  // axios.post('/check-rtmp-cred')
  // let session = nms.getSession(id);
  // session.reject();
});

// nms.on('postPublish', (id, StreamPath, args) => {
//   console.log('[NodeEvent on postPublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
// });

// nms.on('donePublish', (id, StreamPath, args) => {
//   console.log('[NodeEvent on donePublish]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
// });

// nms.on('prePlay', (id, StreamPath, args) => {
//   console.log('[NodeEvent on prePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
//   // let session = nms.getSession(id);
//   // session.reject();
// });

// nms.on('postPlay', (id, StreamPath, args) => {
//   console.log('[NodeEvent on postPlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
// });

// nms.on('donePlay', (id, StreamPath, args) => {
//   console.log('[NodeEvent on donePlay]', `id=${id} StreamPath=${StreamPath} args=${JSON.stringify(args)}`);
// });

